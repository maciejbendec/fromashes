package models

import play.api.libs.json.Json

/**
  * Created by kprzystalski on 23/04/17.
  */
case class CategoriesREST(id: Long, title: String, description: String)

object CategoriesREST {
  implicit val categoryFormat = Json.format[CategoriesREST]
}

case class ProductsREST(id: Long,catId : Long, title: String, description: String, price: Long)

object ProductsREST {
  implicit val productsFormat = Json.format[ProductsREST]
}

case class TypesREST(id: Long,prodId: Long, title: String, info: String, extraPrice : Long)

object TypesREST {
  implicit val typesFormat = Json.format[TypesREST]
}

case class ItemsREST(id: Long,typeId : Long, cartId : Option[Long], orderId : Option[Long], count : Long)

object ItemsREST {
  implicit val itemsFormat = Json.format[ItemsREST]
}

case class CartsREST(id: Long,userId : Long)

object CartsREST {
  implicit val cartsFormat = Json.format[CartsREST]
}

case class OrdersREST(id: Long,userId : Long)

object OrdersREST {
  implicit val ordersFormat = Json.format[OrdersREST]
}

case class UsersREST(id: Long,name: String)

object UsersREST {
  implicit val usersFormat = Json.format[UsersREST]
}