package models

import java.sql.Timestamp

import play.api.libs.json.Format

/**
  * Created by kprzystalski on 23/04/17.
  */
case class Categories(id: Long,title: String, description: String)

case class Products(id: Long, catId : Long, title: String, description: String,price: Long)

case class Types(id: Long, prodId: Long, extraPrice : Long,title: String, info: String )

case class Items(id: Long, typeId : Long, cartId : Option[Long], orderId : Option[Long], count : Long)

case class Carts(id: Long, userId : Long)

case class Orders(id: Long, userId : Long)

case class Users(id: Long, name: String)
