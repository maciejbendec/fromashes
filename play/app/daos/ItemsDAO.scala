package daos

import javax.inject.Inject

import models.{Items, ItemsREST}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.driver.JdbcProfile
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{ExecutionContext, Future}

/**
  * Created by kprzystalski on 23/04/17.
  */

class ItemsDAO @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)
  extends HasDatabaseConfigProvider[JdbcProfile] {

  import driver.api._

  val Items = TableQuery[ItemsTable]

  def all(implicit ec: ExecutionContext): Future[List[ItemsREST]] = {
    val query =  Items
    val results = query.result
    val futureItems = db.run(results)
    futureItems.map(
      _.map {
        a => ItemsREST(id=a.id,typeId=a.typeId, cartId=a.cartId, orderId=a.orderId,count=a.count)
      }.toList)
  }

  def insert(item: Items): Future[Unit] = db.run(Items += item).map { _ => () }

  def get(id: Long): Future[Option[Items]] =
  {
    val query = Items.filter(_.id === id)
    val result = query.result
    db.run(result.headOption)
  }
  def update(item: Items): Future[Unit] = db.run {
    val query = for { i <- Items if i.id === item.id } yield i
    query.update(item).map(_ => ())
  }

  def remove(id: Long): Future[Unit] = db.run {
    println("Usuwam"+id)
    val query = for { i <- Items if i.id === id } yield i
    query.delete.map(_ => ())
  }
  def getByCart(cartId: Long): Future[List[ItemsREST]] =
  {
    val query = Items.filter(_.cartId === cartId)
    val results = query.result
    val futureItems = db.run(results)

    futureItems.map(
      _.map
      {
        a => ItemsREST(id=a.id,typeId = a.typeId, cartId = a.cartId, orderId=a.orderId, count=a.count)
      }.toList
    )
  }
  def getByOrder(orderId: Long): Future[List[ItemsREST]] =
  {
    val query = Items.filter(_.orderId === orderId)
    val results = query.result
    val futureItems = db.run(results)

    futureItems.map(
      _.map
      {
        a => ItemsREST(id=a.id,typeId = a.typeId, cartId = a.cartId, orderId=a.orderId, count=a.count)
      }.toList
    )
  }
  def clearByCart(cartId: Long): Future[Unit] = db.run {
    val query = for { i <- Items if i.cartId === cartId } yield i
    query.delete.map(_ => ())
  }
  class ItemsTable(tag: Tag) extends Table[Items](tag, "Items") {
    def id = column[Long]("id",O.AutoInc, O.AutoInc)
    def typeId = column[Long]("typeId")
    def cartId = column[Option[Long]]("cartId")
    def orderId = column[Option[Long]]("orderId")
    def count = column[Long]("count")
    def * = (id,typeId, cartId, orderId,count) <> (models.Items.tupled, models.Items.unapply)
  }

}
