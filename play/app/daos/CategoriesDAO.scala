package daos

import javax.inject.Inject

import models.{Categories, CategoriesREST}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.driver.JdbcProfile
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{ExecutionContext, Future}

/**
  * Created by kprzystalski on 23/04/17.
  */

class CategoriesDAO @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)
  extends HasDatabaseConfigProvider[JdbcProfile] {

  import driver.api._

  val Categories = TableQuery[CategoriesTable]

  def all(implicit ec: ExecutionContext): Future[List[CategoriesREST]] = {
    val query =  Categories
    val results = query.result
    val futureCategories = db.run(results)
    futureCategories.map(
      _.map {
        a => CategoriesREST(id=a.id,title = a.title, description= a.description)
      }.toList)
  }

  def insert(category: Categories): Future[Unit] = db.run(Categories += category).map { _ => () }

  def get(id: Long): Future[Option[Categories]] =
  {
    val query = Categories.filter(_.id === id)
    val result = query.result
    db.run(result.headOption)
  }

  class CategoriesTable(tag: Tag) extends Table[Categories](tag, "Categories") {
    def id = column[Long]("id",O.AutoInc, O.AutoInc)
    def title = column[String]("title")
    def description = column[String]("description")
    def * = (id, title, description) <> (models.Categories.tupled, models.Categories.unapply)
  }

}
