package daos

import javax.inject.Inject

import models.{Carts, CartsREST}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.driver.JdbcProfile
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{ExecutionContext, Future}

/**
  * Created by kprzystalski on 23/04/17.
  */

class CartsDAO @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)
  extends HasDatabaseConfigProvider[JdbcProfile] {

  import driver.api._

  val Carts = TableQuery[CartsTable]

  def all(implicit ec: ExecutionContext): Future[List[CartsREST]] = {
    val query =  Carts
    val results = query.result
    val futureCarts = db.run(results)
    futureCarts.map(
      _.map {
        a => CartsREST(id=a.id,userId=a.userId)
      }.toList)
  }

  def insert(cart: Carts): Future[Unit] = db.run(Carts += cart).map { _ => () }

  def get(id: Long): Future[Option[Carts]] =
  {
    val query = Carts.filter(_.id === id)
    val result = query.result
    println(result)
    db.run(result.headOption)
  }

  def getByUser(userId: Long): Future[List[CartsREST]] =
  {
    val query = Carts.filter(_.userId === userId)
    val results = query.result
    val futureCarts = db.run(results)

    futureCarts.map(
      _.map
      {
        a => CartsREST(id=a.id,userId = a.userId)
      }.toList
    )
  }

  class CartsTable(tag: Tag) extends Table[Carts](tag, "Carts") {
    def id = column[Long]("prodId",O.AutoInc, O.AutoInc)
    def userId= column[Long]("userId")
    def * = (id,userId) <> (models.Carts.tupled, models.Carts.unapply)
  }

}