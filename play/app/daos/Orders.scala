package daos

import javax.inject.Inject

import models.{Orders, OrdersREST}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.driver.JdbcProfile
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{ExecutionContext, Future}

/**
  * Created by kprzystalski on 23/04/17.
  */

class OrdersDAO @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)
  extends HasDatabaseConfigProvider[JdbcProfile] {

  import driver.api._

  val Orders = TableQuery[OrdersTable]

  def all(implicit ec: ExecutionContext): Future[List[OrdersREST]] = {
    val query =  Orders
    val results = query.result
    val futureOrders = db.run(results)
    futureOrders.map(
      _.map {
        a => OrdersREST(id=a.id,userId = a.userId)
      }.toList)
  }

  def insert(order: Orders): Future[Unit] = db.run(Orders += order).map { _ => () }

  def get(id: Long): Future[Option[Orders]] =
  {
    val query = Orders.filter(_.id === id)
    val result = query.result
    db.run(result.headOption)
  }

  def update(order: Orders): Future[Unit] = db.run {
    val query = for { i <- Orders if i.id === order.id } yield i
    query.update(order).map(_ => ())
  }

  def remove(id: Long): Future[Unit] = db.run {
    val query = for { i <- Orders if i.id === id } yield i
    query.delete.map(_ => ())
  }

  def getByUser(userId: Long): Future[List[OrdersREST]] =
  {
    val query = Orders.filter(_.userId === userId)
    val results = query.result
    val futureOrders = db.run(results)

    futureOrders.map(
      _.map
      {
        a => OrdersREST(id=a.id,userId = a.userId)
      }.toList
    )
  }

  class OrdersTable(tag: Tag) extends Table[Orders](tag, "Orders") {
    def id = column[Long]("prodId",O.AutoInc, O.AutoInc)
    def userId = column[Long]("userId")
    def * = (id,userId) <> (models.Orders.tupled, models.Orders.unapply)
  }

}