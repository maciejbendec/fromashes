package daos

import javax.inject.Inject

import models.{Users, UsersREST}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.driver.JdbcProfile
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{ExecutionContext, Future}

/**
  * Created by kprzystalski on 23/04/17.
  */

class UsersDAO @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)
  extends HasDatabaseConfigProvider[JdbcProfile] {

  import driver.api._

  val Users = TableQuery[UsersTable]

  def all(implicit ec: ExecutionContext): Future[List[UsersREST]] = {
    val query =  Users
    val results = query.result
    val futureUsers = db.run(results)
    futureUsers.map(
      _.map {
        a => UsersREST(id=a.id,name=a.name)
      }.toList)
  }

  def insert(user: Users): Future[Unit] = db.run(Users += user).map { _ => () }

  def get(id: Long): Future[Option[Users]] =
  {
    val query = Users.filter(_.id === id)
    val result = query.result
    db.run(result.headOption)
  }

  class UsersTable(tag: Tag) extends Table[Users](tag, "Users") {
    def id = column[Long]("id",O.AutoInc, O.AutoInc)
    def name = column[String]("name")
    def * = (id,name) <> (models.Users.tupled, models.Users.unapply)
  }

}
