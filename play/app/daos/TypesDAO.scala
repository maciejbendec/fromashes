package daos

import javax.inject.Inject

import models.{Types, TypesREST}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.driver.JdbcProfile
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{ExecutionContext, Future}

/**
  * Created by kprzystalski on 23/04/17.
  */

class TypesDAO @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)
  extends HasDatabaseConfigProvider[JdbcProfile] {

  import driver.api._

  val Types = TableQuery[TypesTable]

  def all(implicit ec: ExecutionContext): Future[List[TypesREST]] = {
    val query =  Types
    val results = query.result
    val futureTypes = db.run(results)
    futureTypes.map(
      _.map {
        a => TypesREST(id=a.id,prodId=a.prodId, extraPrice = a.extraPrice,title=a.title, info = a.info)
      }.toList)
  }

  def insert(mytype: Types): Future[Unit] = db.run(Types += mytype).map { _ => () }

  def get(id: Long): Future[Option[Types]] =
  {
    val query = Types.filter(_.id === id)
    val result = query.result
    db.run(result.headOption)
  }

  def getByProduct(prodId: Long): Future[List[TypesREST]] =
  {
    val query = Types.filter(_.prodId === prodId)
    val results = query.result
    val futureItems = db.run(results)

    futureItems.map(
      _.map
      {
        a => TypesREST(id=a.id,prodId = a.prodId, extraPrice = a.extraPrice, title = a.title, info= a.info)
      }.toList
    )
  }
  class TypesTable(tag: Tag) extends Table[Types](tag, "Types") {
    def id = column[Long]("id",O.AutoInc, O.AutoInc)
    def prodId = column[Long]("prodId")
    def extraPrice = column[Long]("extraPrice")
    def title = column[String]("title")
    def info = column[String]("info")
    def * = (id,prodId, extraPrice,title, info) <> (models.Types.tupled, models.Types.unapply)
  }

}
