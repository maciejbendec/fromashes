package daos

import javax.inject.Inject

import models.{Products, ProductsREST}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.driver.JdbcProfile
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{ExecutionContext, Future}

/**
  * Created by kprzystalski on 23/04/17.
  */

class ProductsDAO @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)
  extends HasDatabaseConfigProvider[JdbcProfile] {

  import driver.api._

  val Products = TableQuery[ProductsTable]

  def all(implicit ec: ExecutionContext): Future[List[ProductsREST]] = {
    val query =  Products
    val results = query.result
    val futureProducts = db.run(results)
    futureProducts.map(
      _.map {
        a => ProductsREST(id=a.id,description = a.description, title = a.title, catId=a.catId, price=a.price)
      }.toList)
  }

  def insert(product: Products): Future[Unit] = db.run(Products += product).map { _ => () }

  def get(id: Long): Future[Option[Products]] =
  {
    val query = Products.filter(_.id === id)
    val result = query.result
    db.run(result.headOption)
  }

  def getByCategory(catId: Long): Future[List[ProductsREST]] =
  {
    val query = Products.filter(_.catId === catId)
    val results = query.result
    val futureProducts = db.run(results)

    futureProducts.map(
      _.map
      {
        a => ProductsREST(id=a.id,description = a.description, title = a.title, catId=a.catId, price=a.price)
      }.toList
    )
  }

  class ProductsTable(tag: Tag) extends Table[Products](tag, "Products") {
    def id = column[Long]("id",O.AutoInc, O.AutoInc)
    def title = column[String]("title")
    def description = column[String]("description")
    def catId= column[Long]("catId")
    def price= column[Long]("price")
    def * = (id,catId, title, description,price) <> (models.Products.tupled, models.Products.unapply)
  }

}
