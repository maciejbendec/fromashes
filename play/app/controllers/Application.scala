package controllers

import javax.inject.Inject

import daos.{CartsDAO, CategoriesDAO, ItemsDAO, OrdersDAO, ProductsDAO, TypesDAO, UsersDAO}
import models.{CartsREST, CategoriesREST, ItemsREST, OrdersREST, ProductsREST, TypesREST, UsersREST}
import models.{Carts, Categories, Items, Orders, Products, Types, Users}
import play.api.libs.json.{JsValue, Json, Writes}
import play.api.mvc._
import play.api.libs.concurrent.Execution.Implicits.defaultContext

class Application @Inject() (categoriesDAO: CategoriesDAO,productsDAO: ProductsDAO,typesDAO: TypesDAO,
                             itemsDAO: ItemsDAO,cartsDAO: CartsDAO,ordersDAO: OrdersDAO,
                             usersDAO: UsersDAO) extends Controller {
  implicit val categoriesWrites = new Writes[Option[models.Categories]]
  {
    def writes(option: Option[Categories]) = Json.obj(
      "id"	-> option.get.id,
      "title"		-> option.get.title,
      "description"		-> option.get.description
    )
  }
  implicit val productsWrites = new Writes[Option[models.Products]]
  {
    def writes(option: Option[Products]) = Json.obj(
      "id"	-> option.get.id,
      "title"		-> option.get.title,
      "description"		-> option.get.description,
      "price" -> option.get.price,
      "catId"		-> option.get.catId
    )
  }
  implicit val typesWrites = new Writes[Option[models.Types]]
  {
    def writes(option: Option[Types]) = Json.obj(
      "id"	-> option.get.id,
      "prodId" -> option.get.prodId,
      "extraPrice" -> option.get.extraPrice,
      "title"		-> option.get.title,
      "info"		-> option.get.info
    )
  }
  implicit val itemsWrites = new Writes[Option[models.Items]]
  {
    def writes(option: Option[Items]) = Json.obj(
      "id"	-> option.get.id,
      "typeId"		-> option.get.typeId,
      "cartId"		-> option.get.cartId,
      "orderId" -> option.get.orderId,
      "count"		-> option.get.count
    )
  }
  implicit val ordersWrites = new Writes[Option[models.Orders]]
  {
    def writes(option: Option[Orders]) = Json.obj(
      "id"	-> option.get.id,
      "userId"		-> option.get.userId
    )
  }
  implicit val cartsWrites = new Writes[Option[models.Carts]]
  {
    def writes(option: Option[Carts]) = Json.obj(
      "id"	-> option.get.id,
      "userId"		-> option.get.userId
    )
  }
  implicit val usersWrites = new Writes[Option[models.Users]]
  {
    def writes(option: Option[Users]) = Json.obj(
      "id"	-> option.get.id,
      "name"		-> option.get.name
    )
  }

  def index = Action.async { implicit  request =>
    productsDAO.all map {
      products => Ok(Json.toJson(products))
    }
  }

  def newproduct = Action { implicit request =>
    var json:ProductsREST = request.body.asJson.get.as[ProductsREST]
    var product = Products(id = 0, catId=json.catId,title = json.title, description = json.description,price=json.price)
    productsDAO.insert(product)
    Ok(request.body.asJson.get)
  }

  def allProducts() = Action.async { implicit  request =>
    productsDAO.all map {
      products => Ok(Json.toJson(products))
    }
  }

  def getProduct(id: Long) = Action.async { implicit request =>
    var product : JsValue = Json.obj()
    productsDAO.get(id) map
      { products =>
        product = Json.toJson(products)
      }
    typesDAO.getByProduct(id) map
      { types =>
        Json.toJson(types)
        Ok(Json.obj(
          "product" -> product,
          "types" -> types
        ));
      }
  }

  //def getProductByCategory(id: Long) = play.mvc.Results.TODO

  //def getTypesByProduct(id: Long) = play.mvc.Results.TODO

  def getType(id: Long) = Action.async { implicit request =>
    typesDAO.get(id) map
      { types =>
        Json.toJson(types)
        Ok(Json.obj(
          "type" -> types
        ));
      }

  }

  def getItem(id: Long) = Action.async { implicit request =>
    itemsDAO.get(id) map
      { items =>
        Json.toJson(items)
        Ok(Json.obj(
          "item" -> items
        ));
      }
  }

  //def getItemsByOrder(id: Long) = play.mvc.Results.TODO

  //def getItemsByCart(id: Long) = play.mvc.Results.TODO

  def newItem() = Action { implicit request =>
    var json:ItemsREST = request.body.asJson.get.as[ItemsREST]
    var item = Items(id = 0, typeId=json.typeId,cartId = json.cartId, orderId = json.orderId,count=json.count)
    itemsDAO.insert(item)
    Ok(request.body.asJson.get)
  }

  def updateItem(id: Long) = Action
  { implicit request =>
    var json: ItemsREST = request.body.asJson.get.as[ItemsREST]
    var item = Items(id = id,typeId=json.typeId,cartId = json.cartId, orderId = json.orderId,count=json.count)
    itemsDAO.update(item)
    Ok(request.body.asJson.get)
  }

  def removeItem(id: Long) = Action
  { implicit request =>
    itemsDAO.remove(id)
    Ok("")
  }

  def allCategories() = Action.async { implicit  request =>
    categoriesDAO.all map {
      categories => Ok(Json.toJson(categories))
    }
  }

  def getCategory(id: Long) = Action.async
  { implicit request =>
    var category : JsValue = Json.obj()
    categoriesDAO.get(id) map
      { categories =>
        category = Json.toJson(categories)
      }
    productsDAO.getByCategory(id) map
      { products =>
        Json.toJson(products)
        Ok(Json.obj(
          "category" -> category,
          "products" -> products
        ));
      }
  }
  def getCart(id: Long) = Action.async { implicit request =>
    var cart : JsValue = Json.obj()
    cartsDAO.get(id) map
      { carts =>
        cart = Json.toJson(carts)
      }
    itemsDAO.getByCart(id) map
      { items =>
        Json.toJson(items)
        Ok(Json.obj(
          "cart" -> cart,
          "items" -> items
        ));
      }
  }

  //def getCartByUser(id: Long) = play.mvc.Results.TODO

  def clearCart(id: Long) = Action { implicit request =>
    itemsDAO.clearByCart(id)
    Ok("")
  }

  def getOrder(id: Long) = Action.async { implicit request =>
    var order : JsValue = Json.obj()
    ordersDAO.get(id) map
      { orders =>
        order = Json.toJson(orders)
      }
    itemsDAO.getByOrder(id) map
      { items =>
        Json.toJson(items)
        Ok(Json.obj(
          "order" -> order,
          "items" -> items
        ));
      }

  }

  //def getOrderByUser(id: Long) = play.mvc.Results.TODO

  def newOrder() = Action { implicit request =>
    var json:OrdersREST = request.body.asJson.get.as[OrdersREST]
    var order = Orders(id = 0, userId=json.userId)
    ordersDAO.insert(order)
    Ok(request.body.asJson.get)
  }

  def updateOrder(id: Long) = Action
  { implicit request =>
    var json: OrdersREST = request.body.asJson.get.as[OrdersREST]
    var order = Orders(id = id, userId=json.userId)
    ordersDAO.update(order)
    Ok(request.body.asJson.get)
  }

  def removeOrder(id: Long) = Action
  { implicit request =>
    ordersDAO.remove(id)
    Ok("")
  }

  def allUsers() = Action.async { implicit  request =>
    usersDAO.all map {
      users => Ok(Json.toJson(users))
    }
  }

  def getUser(id: Long) = Action.async { implicit request =>
    var user : JsValue = Json.obj()
    var order_r: JsValue = Json.obj()
    usersDAO.get(id) map
      { users =>
        user = Json.toJson(users)
      }
    ordersDAO.getByUser(id) map
      { orders =>
        order_r=Json.toJson(orders)
      }
    cartsDAO.getByUser(id) map
      { carts =>
        Json.toJson(carts)
        Ok(Json.obj(
          "user" -> user,
          "orders" -> order_r,
          "carts" -> carts
        ));
      }
  }
}
