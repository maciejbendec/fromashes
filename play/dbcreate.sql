/*User dump*/
insert into users values (0,'Maciej');
/*Carts dump*/
insert into carts values ('0','0');
/*Orders dump*/
insert into orders values ('0','0');
/*Categories dump*/
insert into categories values ('0','Biurka','Drewniane i metalowe biurka');
insert into categories values ('1','Krzesła','Takie coś do siedzenia');
insert into categories values ('2','Fotele','Też do siedzenia ale droższe');
/*Products dump*/
insert into products values ('0','Biurko1','Małe drewniane biurko','0','200');
insert into products values ('1','Biurko2','Duże drewniane biurko','0','400');
insert into products values ('2','Krzesło1','Brzydkie drewniane krzesło','1','100');
insert into products values ('3','Krzesło2','Ładne drewniane krzesło','1','150');
insert into products values ('4','Fotel1','Zwykły fotel','2','1000');
insert into products values ('5','Fotel2','Luksusowy fotel','2','10000');
/*Types dump*/
insert into types values ('0','0','0','Sosnowe','Tak naprawdę to płyta wiórowa');
insert into types values ('1','0','200','Hebanowe','I tak cię nie stać');
insert into types values ('2','1','0','Sosnowe','Tak naprawdę to płyta wiórowa');
insert into types values ('3','1','200','Hebanowe','I tak cię nie stać');
insert into types values ('4','2','0','Sosnowe','Tak naprawdę to płyta wiórowa');
insert into types values ('5','2','400','Hebanowe','I tak cię nie stać');
insert into types values ('6','3','0','Sosnowe','Tak naprawdę to płyta wiórowa');
insert into types values ('7','3','400','Hebanowe','I tak cię nie stać');
insert into types values ('8','4','0','Eko skóra','Udawanie burżuja');
insert into types values ('9','4','500','Eko skóra','Żadne zwierzę nie ucierpiało, naprawdę!');
insert into types values ('10','5','0','Eko skóra','Udawanie burżuja');
insert into types values ('11','5','2000','Eko skóra','Żadne zwierzę nie ucierpiało, naprawdę!');
/*Items dump*/
insert into items values ('0','0','0',null,'1');
insert into items values ('1','0',null,'0','1');
