export class Product {
  id: number;
  title: string;
  description: string;
  catId: number;
  price: number;
}
