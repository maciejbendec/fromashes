import { TestBed, inject } from '@angular/core/testing';

import { CategorydetailsService } from './categorydetails.service';

describe('CategorydetailsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CategorydetailsService]
    });
  });

  it('should be created', inject([CategorydetailsService], (service: CategorydetailsService) => {
    expect(service).toBeTruthy();
  }));
});
