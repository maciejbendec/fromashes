import { Injectable } from '@angular/core';
import {Http, Headers, RequestOptions} from "@angular/http";
import {Category} from "../category/category";

@Injectable()
export class CategorydetailsService {

  constructor(private http: Http) {
  }

  getCategory(id: number) {
    const headers: Headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');

    const options = new RequestOptions({headers: headers});
    console.log(id);
    return this.http.get('http://localhost:9000/category/' + id, options).map(response => <any>response.json());
  }

}
