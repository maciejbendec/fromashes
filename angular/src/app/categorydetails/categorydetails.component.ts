import { Component, OnInit } from '@angular/core';
import {Category} from "../category/category";
import {Product} from "../product/product";
import {ActivatedRoute} from "@angular/router";
import {CategorydetailsService} from "./categorydetails.service";

@Component({
  selector: 'app-categorydetails',
  templateUrl: './categorydetails.component.html',
  styleUrls: ['./categorydetails.component.css']
})
export class CategorydetailsComponent implements OnInit {

  category: Category;
  products: Product[];
  id: number;
  private sub: any;
  constructor(private CategorydetailsService: CategorydetailsService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params =>
    {
      this.id = +params['id'];
    });
    this.CategorydetailsService.getCategory(this.id).subscribe(data =>
    {
      this.category = data.category;
      this.products = data.products;
      console.log("Dupa");
    });

    console.log(this.route.snapshot.params);
  }

}
