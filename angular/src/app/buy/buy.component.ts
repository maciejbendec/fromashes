import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl} from "@angular/forms";
import {ActivatedRoute} from "@angular/router";
import {BuyService} from "./buy.service";

@Component({
  selector: 'app-buy',
  templateUrl: './buy.component.html',
  styleUrls: ['./buy.component.css']
})
export class BuyComponent implements OnInit {

  //products: Product[];

  payment: FormGroup;

  delivery: FormGroup;
  constructor(private buyService: BuyService, private route: ActivatedRoute) { }

  ngOnInit() {
    //this.buyService.getProducts().subscribe(data => this.products = data);

    this.payment = new FormGroup({
      karta: new FormControl('karta'),
      gotowka: new FormControl('gotowka'),
      nerka: new FormControl('nerka')
    });

    this.delivery = new FormGroup({
      kurier: new FormControl('kurier'),
      paczkomat: new FormControl('paczkomat')
    });
    console.log(this.route.snapshot.params);
  }

  /*addProduct(event) {
    console.log(event);
    console.log(this.productForm.value);
    this.productService.sendToPlay(this.productForm.value);
  }*/

}
