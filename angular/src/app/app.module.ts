import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpModule } from '@angular/http';


import { AppComponent } from './app.component';
import { ProductComponent } from './product/product.component';
import {RouterModule} from "@angular/router";
import {ProductService} from "./product/product.service";
import { CategoryComponent } from './category/category.component';
import {CategoryService} from "./category/category.service";
import { ProductDetailsComponent } from './productdetails/productdetails.component';
import {ProductDetailsService} from "./productdetails/productdetails.service";
import { CategorydetailsComponent } from './categorydetails/categorydetails.component';
import {CategorydetailsService} from "./categorydetails/categorydetails.service";
import { ItemComponent } from './item/item.component';
import {ItemService} from "./item/item.service";
import { CartComponent } from './cart/cart.component';
import {CartService} from "./cart/cart.service";
import { BuyComponent } from './buy/buy.component';
import {BuyService} from "./buy/buy.service";
import {Ng2UiAuthModule, CustomConfig} from 'ng2-ui-auth';
export const GOOGLE_CLIENT_ID = '811003490402-euu89dhnlikjnatoaiknfljkdeg85sh7.apps.googleusercontent.com';
export class MyAuthConfig extends CustomConfig {
  defaultHeaders = {'Content-Type': 'application/json'};
  providers =
    {
      google:
        {
          clientId: GOOGLE_CLIENT_ID,
          url: 'http://localhost:9000/authenticate/google'
        },
      twitter:
        {
          //clientId: '1905555483065495',
          url: 'http://localhost:9000/authenticate/twitter'
        }
    };
}
@NgModule({
  declarations: [
    AppComponent,
    ProductComponent,
    CategoryComponent,
    ProductDetailsComponent,
    CategorydetailsComponent,
    ItemComponent,
    CartComponent,
    BuyComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    Ng2UiAuthModule.forRoot(MyAuthConfig),
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      { path: '.', component: AppComponent},
      { path: 'products', component: ProductComponent},
      { path: 'product/:id', component: ProductDetailsComponent},
      { path: 'categories', component: CategoryComponent},
      { path: 'category/:id', component: CategorydetailsComponent},
      { path: 'item/:id', component: ItemComponent},
      { path: 'cart/:id', component: CartComponent},
      { path: 'buy/:id', component: BuyComponent}
      ])
  ],
  providers: [ProductService,CategoryService,ProductDetailsService,CategorydetailsService,ItemService,CartService,BuyService],
  bootstrap: [AppComponent]
})
export class AppModule { }
