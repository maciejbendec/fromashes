/**
 * Created by maciej on 19.06.17.
 */
export class Category {
  title: string;
  description: string;
}
