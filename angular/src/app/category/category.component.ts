import { Component, OnInit } from '@angular/core';
import {CategoryService} from './category.service';
import {ActivatedRoute} from "@angular/router";
import {Category} from './category';
@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  categories: Category[];
  constructor(private categoryService: CategoryService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.categoryService.getCategories().subscribe(data => this.categories = data);

    /*this.productForm = new FormGroup({
      tytul: new FormControl('tytul', Validators.required),
      opis: new FormControl('opis', Validators.required),
      catId: new FormControl('catId', Validators.required),
      cena: new FormControl('cena', Validators.required)
    });*/

    console.log(this.route.snapshot.params);
  }

}
