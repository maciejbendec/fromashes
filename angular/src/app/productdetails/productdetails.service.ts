import { Injectable } from '@angular/core';
import {Http, Headers, RequestOptions} from "@angular/http";
import {ProductDetails} from "./productdetails";
import 'rxjs/add/operator/map';

@Injectable()
export class ProductDetailsService {

  constructor(private http: Http) { }

  getProduct(id: number)
  {
    const headers: Headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');

    const options = new RequestOptions({headers: headers});
    console.log(id);
    return this.http.get('http://localhost:9000/product/' + id, options).map(response => <any>response.json());
  }

  getTypeByItem(id: number)
  {
    const headers: Headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    const options = new RequestOptions({headers: headers});
    console.log(id);
    return this.http.get('http://localhost:9000/type/' + id, options).map(response => <any>response.json());
  }

  getProductByType(id: number)
  {
    const headers: Headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    const options = new RequestOptions({headers: headers});
    console.log(id);
    return this.http.get('http://localhost:9000/product/' + id, options).map(response => <any>response.json());
  }

}
