export class ProductDetails {
  id : number;
  title: string;
  description: string;
  catId: number;
  price: number;
}
export class Types {
  id : number;
  prodId: number;
  extraPrice: number;
  title: string;
  info: string;
}
