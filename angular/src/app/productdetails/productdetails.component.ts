import { Component, OnInit } from '@angular/core';
import {ProductDetailsService} from "./productdetails.service";
import {ActivatedRoute} from "@angular/router";
import {ProductDetails, Types} from "./productdetails";
import {ItemService} from "../item/item.service";

@Component({
  selector: 'app-productdetails',
  templateUrl: './productdetails.component.html',
  styleUrls: ['./productdetails.component.css']
})
export class ProductDetailsComponent implements OnInit {

  productDetails: ProductDetails;
  types: Types[];
  id: number;
  private sub: any;
  constructor(private productDetailsService: ProductDetailsService,private itemService: ItemService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params =>
    {
      this.id = +params['id'];
    });
    this.productDetailsService.getProduct(this.id).subscribe(data =>
    {
      this.productDetails = data.product;
      this.types = data.types;
      console.log("Dupa");
      console.log(this.productDetails);
    });

    console.log(this.route.snapshot.params);
  }
  addToCart(typeId,cartId,orderId,count){
    this.itemService.sendToPlay(typeId,cartId,orderId,count);
  }
}
