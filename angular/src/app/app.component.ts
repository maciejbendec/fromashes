import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import { JwtHttp, AuthService } from 'ng2-ui-auth';
import {Headers, RequestOptions, Http} from "@angular/http";
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit
{
  constructor(private auth: AuthService, private router: Router,private http: Http) { }
  user: any;
  ngOnInit()
  {
    console.log(this.auth.isAuthenticated());
    this.getUser().subscribe(data => {
        this.user = data;
        this.user = JSON.parse(this.user._body);
      },
      err =>{
        console.error(err);
      });
  }

  authenticate(provider)
  {
    console.log("works");
    this.auth.authenticate(provider).subscribe(
      {
        error: (err: any) => console.log(err),
        complete: () => this.router.navigateByUrl('')
      });
  };

  getUser()
  {
    const headers: Headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('X-Auth-Token', this.auth.getToken());

    const options = new RequestOptions({headers: headers});

    return this.http.get('http://localhost:9000/user', options);
  }

  isAuthenticated(){
    return this.auth.isAuthenticated();
  }

}
