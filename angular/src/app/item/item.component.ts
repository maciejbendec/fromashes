import { Component, OnInit } from '@angular/core';
import {Item} from "./item";
import {ItemService} from "./item.service";
import {ActivatedRoute} from "@angular/router";
import {FormGroup} from "@angular/forms";

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {

  item: Item;
  itemForm: FormGroup;
  id: number;
  private sub: any;
  constructor(private itemService: ItemService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params =>
    {
      this.id = +params['id'];
    });
    this.itemService.getItem(this.id).subscribe(data =>
    {
      this.item= data.item;
    });

    console.log(this.route.snapshot.params);
  }

  editItem(event) {
    console.log(event);
    console.log(this.itemForm.value);
    this.itemService.updateToPlay(this.id,this.item);
  }
  deleteItem(event) {
    console.log(event);
    console.log(this.itemForm.value);
    this.itemService.deleteToPlay(this.id);
  }
}
