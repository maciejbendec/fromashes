/**
 * Created by maciej on 24.06.17.
 */
export class Item {
  id: number;
  typeId: number;
  cartId: number;
  orderId: number;
  count: number;
}
