import { Injectable } from '@angular/core';
import {Http, Headers, RequestOptions} from "@angular/http";

@Injectable()
export class ItemService {

  constructor(private http: Http) {
  }

  getItem(id: number) {
    const headers: Headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');

    const options = new RequestOptions({headers: headers});
    console.log(id);
    return this.http.get('http://localhost:9000/item/' + id, options).map(response => <any>response.json());
  }

  sendToPlay(typeId,cartId,orderId,count) {
    if(orderId==-1) orderId=null;
    if(cartId==-1) cartId=null;
    const result = JSON.parse
    (
      '{  "id":' + 0 +', \
          "typeId":' + typeId +', \
          "cartId":' + cartId +', \
          "orderId":' + orderId + ', \
          "count":' + count + '}'
    );
    // console.log(serializedForm);

    const headers: Headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');

    const options = new RequestOptions({headers: headers});

    this.http.post('http://localhost:9000/item', result, options)
      .subscribe(
        data => console.log('wyslane!', data),
        error => console.error('nie bangla', error)
      );
  }

  updateToPlay(id: number,formData) {
    const serializedForm = JSON.stringify(formData);
    // console.log(serializedForm);

    const headers: Headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');

    const options = new RequestOptions({headers: headers});

    this.http.put('http://localhost:9000/item'+ id, serializedForm, options)
      .subscribe(
        data => console.log('wyslane!', data),
        error => console.error('nie bangla', error)
      );
  }

  deleteToPlay(id: number) {
    const headers: Headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');

    const options = new RequestOptions({headers: headers});
    console.log(id);
    this.http.delete('http://localhost:9000/item/' + id, options).subscribe(
      data => {console.log('wyslane!', data);window.location.reload();},
      error => console.error('nie bangla', error)
    );
  }

}
