import { Injectable } from '@angular/core';
import {Http, Headers, RequestOptions} from "@angular/http";

@Injectable()
export class CartService {

  constructor(private http: Http) { }

  getCart(id: number)
  {
    const headers: Headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');

    const options = new RequestOptions({headers: headers});
    console.log(id);
    return this.http.get('http://localhost:9000/cart/' + id, options).map(response => <any>response.json());
  }
}
