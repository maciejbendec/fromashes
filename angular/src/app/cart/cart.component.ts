import { Component, OnInit } from '@angular/core';
import {Item} from "../item/item";
import {Cart} from "./cart";
import {ActivatedRoute} from "@angular/router";
import {CartService} from "./cart.service";
import {ProductDetailsService} from "../productdetails/productdetails.service";
import {ProductDetails, Types} from "../productdetails/productdetails";
import {forEach} from "@angular/router/src/utils/collection";
import {ItemService} from "../item/item.service";

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  cart: Cart;
  items: Item[];
  products: ProductDetails[]=[];
  types: Types[]=[];
  id: number;
  private sub: any;
  constructor(private cartService: CartService,private productDetailsService: ProductDetailsService,private itemService: ItemService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params =>
    {
      this.id = +params['id'];
    });
    this.cartService.getCart(this.id).subscribe(data =>
    {
      this.cart = data.cart;
      this.items = data.items;
      let i=0;
      console.log("BeFor");
      console.log(this.products);
      for (let item of this.items) {
        console.log("For");
        this.productDetailsService.getTypeByItem(item.typeId).subscribe(data => {
          this.productDetailsService.getProductByType(data.type.prodId).subscribe(data2 => {
            console.log("Push");
            this.products.push(data2.product);
            this.types.push(data.type);
            console.log("AfterPush");
          });
        });
      }
    });

    console.log(this.route.snapshot.params);
  }

  deleteFromCart(id:number){
    console.log("delete");
    this.itemService.deleteToPlay(id);
    //this.itemService.deleteToPlay(id);
  }

}
